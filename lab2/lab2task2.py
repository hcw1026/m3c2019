"""Lab 2 Task 2
This module contains functions for simulating Brownian motion
and analyzing the results
"""
import numpy as np
import matplotlib.pyplot as plt

def brown1(Nt,M,dt=0.0001):
    """Run M Brownian motion simulations each consisting of Nt time steps
    with time step = dt
    Returns: X: the M trajectories; Xm: the mean across these M samples; Xv:
    the variance across these M samples
    """
    from numpy.random import randn

    #Initialize variable
    X = np.zeros((M,Nt+1))

    #1D Brownian motion: X_j+1 = X_j + sqrt(dt)*N(0,1)
    for i in range(M):
        for j in range(Nt):
            X[i,j+1] = X[i,j] + np.sqrt(dt)*randn(1)

    Xm = X.mean(axis=0)
    Xv = X.var(axis=0)

    return X,Xm,Xv


def analyze(display=False):
    """Complete this function to analyze simulation error
    """
    
    final_time = 100 # time to get variance
    Nt = 100 # total duration of computation
    dt = 0.0001
    Mvalues = np.arange(50, 50000, 250)
    epsilons = []
    
    for i in range(len(Mvalues)):
        epsilons.append(abs(final_time*dt - brown1(Nt, int(Mvalues[i]))[2][final_time]))
        print('The error at M={} is {:1.3g}'.format(Mvalues[i], epsilons[i]))
        
    if display==True:
        plt.figure()
        plt.plot(np.log(Mvalues), np.log(epsilons))
        


def q2():
    M = 20
    Nt = 100
    
    tra, mean, var = brown1(Nt, M)
    x = np.arange(Nt+1)
    
    if M ==20:
        fig, ax = plt.subplots(5, 4, sharex=True, sharey=True)
        ax = ax.reshape(-1)
        for i in range(M):
            ax[i].plot(x, tra[i])
            ax[i].set_xlabel('time')
            ax[i].set_ylabel('location')
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(x, mean)
    ax.plot(x, var)
    ax.set_xlabel('time')
    ax.set_ylabel('mean/var')
    

        
    