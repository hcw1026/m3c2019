!integrate 4/(1+x^2) using midpoint rule
program mid
    !variable declarations
    integer :: N, i1
    real(kind=8) :: dx, x, f, S
    real(kind=8), parameter :: pi = acos(-1.d0)

    !Set number of intervals
    open(unit=8, file='date.txt')
    read(8,*) N
    close(8)

    !Calculate rectangle width
    dx = 1.d0/dble(N)

    S = 0.d0

    !Iterate over N rectangles
    do i1 = 1,N
        !Compuate x(midpoint)
        x = dx*(dble(i1)-0.5d0)

        !Compute f(x)
        f = 4.d0/(1.d0+x**2)

        !Compute area and add to running sum
        S = S+f*dx

    end do

    subroutine integrand(z,g)
        

    !Output sum
    print *, 'S=',S
    print *, 'error=', abs(S-pi)

end program mid