program second
  implicit none
  integer :: i1,N
  real(kind=8) :: x
  real(kind=8), dimension(100) :: s

  open(unit=8, file='data.txt')
  read(8,*) N
  close(8)

  if (size(s)>=N) then

    call calculations(N, s(1:N))
    print *, 's',s(1:N)

  else 
    print *, 'error, N must be smaller'

  endif

    
  print *, 's=',s(1:i1)

end program second


subroutine calculations(N,s)
  implicit none
  integer, intent(in) :: N
  real(kind=8), dimension(N), intent(inout) :: s
  integer :: i1

  do i1 = 1,N
    s(i1) = sin(dble(i1))
  end do

  print *, 'subroutine, s=',s

end subroutine calculations
