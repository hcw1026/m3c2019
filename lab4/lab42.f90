! Lab 4, task 1

program task1
    implicit none
    integer :: i1
    integer, parameter :: N=5, display_iteration = 1
    real(kind=8) :: odd_sum

    odd_sum = 0.d0

    !logical :: l
    !l = display_iteration == 1

    do i1 = 1, N
        call computeSum(display_iteration, i1, odd_sum)
    end do

end program task1

subroutine computeSum(display, i1, odd_sum)
    
    implicit none
    integer, intent(in):: display, i1
    real(kind=8), intent(out) :: odd_sum

    odd_sum = odd_sum + i1*2.d0 -1.d0

    if (display == 1) then
        print *, 'N=', i1, 'sum=', odd_sum
    end if

end subroutine


! To compile this code:
! $ gfortran -o task1.exe lab41.f90
! To run the resulting executable: $ ./task1.exe
